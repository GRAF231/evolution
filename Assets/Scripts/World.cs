﻿using System;
using Zenject;

public class World : IWorld
{
    [Inject]
    private DangerInfo dangerInfo;

    private int energy = 0;
    private int danger = 0;

    public event Action<int> OnEnergyChange;
    public event Action<int> OnDangerChange;
    public event Action<int> OnDangerLevelChange;

    public int Energy
    {
        get
        {
            return energy;
        }
        set
        {
            energy = value;
            OnEnergyChange?.Invoke(energy);
        }
    }
    public int Danger
    {
        get
        {
            return danger;
        }
        set
        {
            danger = value;
            OnDangerChange?.Invoke(Danger);
            OnDangerLevelChange?.Invoke(DangerLevel);
        }
    }

    public int DangerLevel
    {
        get
        {
            int dLevel = dangerInfo.GetDangerLevel(Danger);
            if (dLevel == dangerInfo.LevelsCount - 1)
            {
                Lose();
                return -1;
            }

            return dLevel;
        }
    }
    void Lose()
    {
        Reset();
    }
    public void Reset()
    {
        Energy = 0;
        Danger = 0;
    }
}
