using UnityEngine;
using Zenject;

[CreateAssetMenu(fileName = "DangerInfoInstaller", menuName = "Installers/DangerInfoInstaller")]
public class DangerInfoInstaller : ScriptableObjectInstaller<DangerInfoInstaller>
{
    [SerializeField] private DangerInfo dangerinfo;
    public override void InstallBindings()
    {
        Container.BindInstance(dangerinfo).IfNotBound();
    }
}