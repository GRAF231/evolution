using UnityEngine;
using Zenject;

public class EvolutionInstaller : MonoInstaller
{
    public override void InstallBindings()
    {
        Container.Bind<IWorld>().To<World>().AsSingle();
        Container.Bind<BuildingManager>().AsSingle();

        //Container.BindFactory<IWorld, BuildingInfo, Building, Building.BuildFactory>();
    }
}