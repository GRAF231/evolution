﻿using UnityEngine;

[CreateAssetMenu(menuName = "Buildings", fileName = "new Building")]
public class BuildingInfo : ScriptableObject
{
    [Tooltip("Вариации постройки")]
    [SerializeField] public GameObject[] prefabs;
    [Tooltip("Иконка в UI")]
    [SerializeField] public Sprite icon;

    [Tooltip("Стоимость")]
    [SerializeField] public int cost;
    [Tooltip("Сколько производит")]
    [SerializeField] public int produce;
    [Tooltip("Опасность")]
    [SerializeField] public int danger;
}
