﻿using UnityEngine;


[CreateAssetMenu(menuName = "Dangers", fileName = "DangerInfo")]
public class DangerInfo : ScriptableObject
{
    [SerializeField] private int[] dangerLevels;
    public int LevelsCount {
        get
        {
            return dangerLevels.Length;
        }
    }
    public int GetDangerLevel(int danger)
    {
        int res = 0;
        for(int i = 0; i < LevelsCount; i++)
        {
            if (dangerLevels[i] <= danger)
                continue;
            else
            {
                res = i;
                return res;
            }
        }
        res = LevelsCount - 1;
        return res;
    }

}
