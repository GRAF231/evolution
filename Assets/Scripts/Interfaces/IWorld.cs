﻿public interface IWorld
{
    event System.Action<int> OnEnergyChange;
    event System.Action<int> OnDangerChange;
    event System.Action<int> OnDangerLevelChange;
    int Energy { get; set; }
    int Danger { get; set; }
    int DangerLevel { get; }
    void Reset();    
}