﻿public interface IBuilding
{
    void Init(BuildingInfo info);
    void OnPlace();
    void OnDestroy();
    void DestroyBuild();
}
