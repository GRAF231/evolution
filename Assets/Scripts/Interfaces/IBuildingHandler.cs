﻿using UnityEngine.EventSystems;

public interface IBuildingHolder : IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{
    IBuilding NestedBuilding
    {
        get;
    }
    void SetNestedBuilding(IBuilding building);

}
