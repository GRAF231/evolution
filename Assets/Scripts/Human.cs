﻿using UnityEngine;
using DG.Tweening;
public class Human : MonoBehaviour
{
    [SerializeField] Transform rotation;
    private bool isRight;
    private float speed;
    // Start is called before the first frame update
    void Start()
    {
        isRight = Random.Range(0, 2) == 0 ? false : true;
        speed = Random.Range(0.1f, 0.5f);
    }

    // Update is called once per frame
    void Update()
    {
        rotation.DOLocalRotate(new Vector3(0, 0, isRight ? 1 : -1), speed ,RotateMode.LocalAxisAdd);
    }
}
