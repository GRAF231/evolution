﻿using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class Planet : MonoBehaviour, IPlanet
{
    [Inject]
    private IWorld world;
    
    [Inject]
    private DangerInfo info; // добавил, чтобы была возможность обратиться к levelsCount и получить в процентном соотношении урвоень опасности (чтобы менять цвет планеты)


    [SerializeField] List<ObjectContainer> buildings;
    [SerializeField] private ObjectContainer defaultBuilding;
    [SerializeField] private GameObject buildingsCont;
    [SerializeField] private GameObject humansCont;
    SpriteRenderer spriteRenderer;

    [ContextMenu("Reset Buildings")]
    public void Reset()
    {
        for (int i = 0; i < buildingsCont.transform.childCount; i++)
        {
            Destroy(buildings[i].GetComponentInChildren<Building>().gameObject);
            buildings[i].GetComponentInChildren<BuildingHolder>().CreateDefaultBuilding();
        }
    }
    public void SetView(int dangerLevel)
    {
        spriteRenderer.color = Color.Lerp(Color.green, Color.red, (float)dangerLevel/(float)info.LevelsCount);
    }
    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        world.OnDangerLevelChange += delegate (int dangerLevel)
        {
            if (dangerLevel == -1)
            {
                Reset();
                SetView(0);
                MenuManager.Instance.Lose();
            }
            SetView(dangerLevel);
        };
    }
    [ContextMenu("Create Buildings")]
    public void CreateBuildings()
    {
        for (int i = 0; i < buildingsCont.transform.childCount; i++)
        {
            Destroy(buildingsCont.transform.GetChild(i).gameObject);
        }
        buildings.Clear();
        for (int i = 0; i < 20; i++)
        {
            buildings.Add(Instantiate(defaultBuilding, buildingsCont.transform));
            buildings[buildings.Count - 1].Rotate.eulerAngles = new Vector3(0, 0, i * 18);
        }
    }
    private void OnEnable()
    {
        world.Reset();
    }
}
