﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingManager
{
    public BuildingInfo currentBuilding;
    private BuildingDrag currentBuildingDrag;

    public BuildingDrag CurrentBuildingDrag
    {
        get
        {
            return currentBuildingDrag;
        }
        set
        {
            if (CurrentBuildingDrag != null)
                currentBuildingDrag.Destroy();
            currentBuildingDrag = value;
        }
    }
    public void Reset()
    {
        currentBuildingDrag.Destroy();
        currentBuildingDrag = null;
        currentBuilding = null;
    }
}
