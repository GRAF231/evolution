﻿using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class BuildingButton : MonoBehaviour
{
    [Inject]
    private BuildingManager buildingManager;
    [Inject]
    private IWorld world;

    [SerializeField] public BuildingInfo info;
    [SerializeField] private Image image;

    private Button button;

    private void Start()
    {
        image.sprite = info.icon;

        button = GetComponent<Button>();

        world.OnEnergyChange += delegate (int energy)
        {
            if (energy >= info.cost)
                button.interactable = true;
            else
                button.interactable = false;
        };
    }
    public void onClick()
    {
        if(HaveEnergy())
            CreateBuildingDrag();
    }
    bool HaveEnergy()
    {
        return info.cost <= world.Energy;
    }
    void CreateBuildingDrag()
    {
        GameObject drag = new GameObject();

        drag.AddComponent<BuildingDrag>();
        SpriteRenderer sr = drag.AddComponent<SpriteRenderer>();
        sr.sprite = image.sprite;


        buildingManager.currentBuilding = info;
        buildingManager.CurrentBuildingDrag = drag.GetComponent<BuildingDrag>();
    }
}
