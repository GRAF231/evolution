﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class EnergyText : MonoBehaviour
{
    private TextMeshProUGUI energy;
    [Inject]
    private IWorld world;
    void Start()
    {
        energy = GetComponent<TextMeshProUGUI>();
        world.OnEnergyChange += delegate (int energy)
        {
            this.energy.text = energy.ToString();
        };
    }
}
