﻿using UnityEngine;

public class BuildingDrag : MonoBehaviour
{
    Transform tr;
    private void Start()
    {
        tr = transform;
        Vector3 mousePos = Input.mousePosition;
        mousePos.z = 0;
        Vector3 pos = Camera.main.ScreenToWorldPoint(mousePos);
        tr.position = new Vector3(pos.x, pos.y, 0);
    }
    void Update()
    {
        Vector3 mousePos = Input.mousePosition;
        mousePos.z = 0;
        Vector3 pos = Camera.main.ScreenToWorldPoint(mousePos);
        tr.position = new Vector3(pos.x, pos.y, 0);
    }

    public void Destroy()
    {
        Destroy(this.gameObject);
    }
}
