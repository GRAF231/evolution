﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIPanel : MonoBehaviour, IPanel
{
    public void ShowUI()
	{
		gameObject.SetActive(true);
	}
	public void HideUI()
	{
		gameObject.SetActive(false);
    }
}
