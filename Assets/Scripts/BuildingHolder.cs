﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

public class BuildingHolder : MonoBehaviour, IBuildingHolder
{
    Sequence mySequence;
    IBuilding nestedBuilding;

    [SerializeField] BuildingInfo defaultBuilding;

    [SerializeField] Building building;

    [Inject]
    BuildingManager buildingManager;
    //[Inject]
    //Building.BuildFactory factory;

    //Я не понял как работать с этой фабрикой, вылетали ошибки, поэтому сделаю вот так
    //Все, что касаетася этой части кода, будет помечено пометкой "Г"-говнокод
    [Inject]
    IWorld world; // Г

    public IBuilding NestedBuilding
    {
        get
        {
            return nestedBuilding;
        }
    }

    void Start()
    {
        mySequence = DOTween.Sequence();
        if (nestedBuilding == null)
            nestedBuilding = building;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        CreateBuilding();
    }
    public void CreateBuilding()
    {
        if (buildingManager.currentBuilding != null)
        {
            GameObject gm = Instantiate(buildingManager.currentBuilding.prefabs[Random.Range(0, buildingManager.currentBuilding.prefabs.Length)], transform);
            Building building = gm.GetComponent<Building>();

            building.SetWorld(world);// Г

            //Building building = factory.Create(world, buildingManager.currentBuilding);

            building.Init(buildingManager.currentBuilding);

            buildingManager.Reset();
            SetNestedBuilding(building);
        }
    }
    public void CreateDefaultBuilding()
    {
            GameObject gm = Instantiate(defaultBuilding.prefabs[Random.Range(0, defaultBuilding.prefabs.Length)], transform);
            Building building = gm.GetComponent<Building>();

            building.SetWorld(world);// Г

            //Building building = factory.Create(world, defaultBuilding);

            building.Init(defaultBuilding);

            SetNestedBuilding(building);
    }
    public void OnPointerEnter(PointerEventData eventData)
    {
        mySequence.Append(transform.DOScale(new Vector2(2f, 2f), .15f).SetLoops(-1))
            .PrependInterval(0.05f)
            .Append(transform.DOScale(new Vector2(1f, 1f), .15f).SetLoops(-1));
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        transform.localScale = new Vector2(1f, 1f);
        transform.DOKill();
    }
    private void OnDisable()
    {
        transform.localScale = new Vector2(1f, 1f);
        transform.DOKill();
    }
    public void SetNestedBuilding(IBuilding building)
    {
        NestedBuilding.DestroyBuild();
        nestedBuilding = building;
        NestedBuilding.OnPlace();
    }
}
