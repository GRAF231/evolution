﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class Building : MonoBehaviour, IBuilding
{
    [Inject] //частично Г
    private IWorld world;

    [SerializeField] private BuildingInfo info;
    SpriteRenderer spriteRenderer;

    //Не Г
    //[Inject]
    //public void Construct(IWorld world)
    //{
    //    this.world = world;
    //}

    void Start()
    {
        if (world == null)
            Debug.Log("null");

        InvokeRepeating("Produce", 1, 1); //запуск производства энергии
    }

    public void SetWorld(IWorld world)
    {
        this.world = world;
    }
    public void Init(BuildingInfo info)
    {
        this.info = info;
    }
    void Produce()
    {
        world.Energy += info.produce;
    }

    public void OnDestroy()
    {
        world.Danger -= info.danger;
    }

    public void OnPlace()
    {
        world.Danger += info.danger;
        world.Energy -= info.cost;
    }

    public void DestroyBuild()
    {
        Destroy(gameObject);
    }

    //public class BuildFactory : Factory<IWorld, BuildingInfo, Building>
    //{
    //}
}
