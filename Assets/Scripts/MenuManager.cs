﻿using UnityEngine;
public class MenuManager : MonoBehaviour
{
    public enum States { MainMenu, PlayPanel, LoseMenu };
	static States currentState = States.MainMenu;
	[SerializeField] private UIPanel[] panels;
    private static MenuManager instance = null;

    [SerializeField] private GameObject[] gameObjects;

    public static MenuManager Instance
    {
        get
        {
            return instance;
        }
    }
    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        instance = this;
        DontDestroyOnLoad(this.gameObject);
    }
    public void ToMenu()
    {
        for (int i = 0; i < gameObjects.Length; i++)
            gameObjects[i].SetActive(false);

        for (int i = 0; i < panels.Length; i++)
            panels[i].HideUI();
        panels[(int)States.MainMenu].ShowUI(); 
    }
    public void Play()
    {
        for (int i = 0; i < gameObjects.Length; i++)
            gameObjects[i].SetActive(true);

        for (int i = 0; i < panels.Length; i++)
            panels[i].HideUI();
        panels[(int)States.PlayPanel].ShowUI();
    }
    public void Lose()
    {
        for (int i = 0; i < gameObjects.Length; i++)
            gameObjects[i].SetActive(false);

        for (int i = 0; i < panels.Length; i++)
            panels[i].HideUI();
        panels[(int)States.LoseMenu].ShowUI();
    }

}

